from flask import Flask, render_template, flash, request
from werkzeug.utils import secure_filename
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = 'my_secret'


class ReusableForm(Form):
    name = TextField('Name:',validators=[validators.required()])
    email = TextField('Email:', validators=[validators.required(), validators.Length(min=6, max=35)])
    Address = TextField('Address:', validators=[validators.required(), validators.Length(min=3, max=35)])

    @app.route("/", methods=['GET', 'POST'])
    def index():
        form = ReusableForm(request.form)  # name of my form

        print(form.errors)
        if request.method == 'POST':   #fetching the details using post method
            name = request.form['name']
            email = request.form['email']
            Address = request.form['Address']


            print(name, " ", email, " ", Address)
            flash("Thanks for registering!")

        return render_template('index.html', form=form)


if __name__ == "__main__":
    app.run()